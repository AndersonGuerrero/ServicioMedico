from django.conf.urls import include, url
from django.contrib import admin
from core.views import SMLogin
from django.views.generic.base import RedirectView
from django.core.urlresolvers import reverse_lazy


urlpatterns = [url(r'^$', RedirectView.as_view(url=reverse_lazy('login'))),
               url(r'^Login/$', SMLogin.as_view(), name='login'),
               url(r'^Logout/$', 'core.views.smlogout', name='logout'),
               url(r'^Admin/', include(admin.site.urls)),
               #url('^User/', include('modules.User.urls')),
               url('^Control/', include('modules.Control.urls')),
               #url('^Report/', include('modules.Report.urls')),
               ]
