from django.contrib import admin
from .models import SMCMedicamento, SMCHistorial, SMCMedicamentoEntregado, SMCEstudiante, SMCTelefono, SMCFacultad

admin.site.register(SMCMedicamento)
admin.site.register(SMCHistorial)
admin.site.register(SMCMedicamentoEntregado)
admin.site.register(SMCEstudiante)
admin.site.register(SMCTelefono)
admin.site.register(SMCFacultad)
