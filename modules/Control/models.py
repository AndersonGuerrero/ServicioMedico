from django.db import models
from modules.User.models import SMUMedico


class SMCFacultad(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return (self.name)

    class Meta:
        db_table = 'smc_facultades'


class SMCEstudiante(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.CharField(max_length=50)
    cedula = models.CharField(max_length=10)
    facultad = models.ForeignKey(SMCFacultad)

    class Meta:
        db_table = 'smc_estudiantes'

    def __unicode__(self):
        return '(%s - %s)' % (self.first_name, self.cedula)


class SMCMedicamento(models.Model):
    nombre = models.CharField(max_length=60)
    compuesto_quimico = models.CharField(max_length=50)
    descripcion = models.TextField(max_length=200)
    existencia = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return '(%s - %s)' % (self.nombre, self.compuesto_quimico)

    class Meta:
        db_table = 'smc_medicamentos'


class SMCHistorial(models.Model):
    paciente = models.ForeignKey(SMCEstudiante)
    medico = models.ForeignKey(SMUMedico)
    fecha_consulta = models.DateField(auto_now_add=True)
    observacion = models.TextField(max_length=200)

    def __unicode__(self):
        return '(%s - %s) - %s' % (self.paciente.first_name, self.paciente.cedula, self.fecha_consulta)

    class Meta:
        db_table = 'smc_historiales'


class SMCMedicamentoEntregado(models.Model):
    historial = models.ForeignKey(SMCHistorial)
    medicamento = models.ForeignKey(SMCMedicamento)
    cantidad_medicamento_entregado = models.PositiveIntegerField()

    def __unicode__(self):
        return '(%s - %s) - (%s - %s)' % (self.historial.fecha_consulta, self.historial.paciente.cedula, self.medicamento.nombre, self.cantidad_medicamento_entregado)

    class Meta:
        db_table = 'smc_medicamentos_entregados'


class SMCTelefono(models.Model):
    estudiante = models.ForeignKey(SMCEstudiante)
    number = models.CharField(max_length=50)

    def __unicode__(self):
        return '(%s - %s)' % (self.estudiante.first_name, self.number)

    class Meta:
        db_table = 'smc_telefonos'
