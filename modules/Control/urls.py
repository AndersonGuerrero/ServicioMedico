from django.conf.urls import url
from .views import SMCMedicamentosList, SMCEstudianteList, SMCEstudianteCreate, SMCEstudianteUpdate
from django.contrib.auth.decorators import login_required
from django.views.generic.base import TemplateView

urlpatterns = [url(r'^$', login_required(TemplateView.as_view(template_name='index.html')), name='index_control'),
               url(r'^Medicamentos/$', login_required(SMCMedicamentosList.as_view()), name='smc_medicamentos_lista'),
               url(r'^Estudiante/$', login_required(SMCEstudianteList.as_view()), name='smc_estudiante_list'),
               url(r'^Estudiante/Create$', login_required(SMCEstudianteCreate.as_view()), name='smc_estudiante_list'),
               url(r'^Estudiante/(?P<pk>[0-9]+)/$', login_required(SMCEstudianteUpdate.as_view()), name='smc_estudiante_list'),
               ]
